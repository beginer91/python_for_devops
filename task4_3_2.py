#!/usr/bin/env python3
import socket
import json
import yaml

servers = {"drive.google.com" : "108.177.14.194", "mail.google.com" : "64.233.162.18", "google.com" : "173.194.220.113"}
servers_result = {}
for i, y in servers.items():
    servers_dict = {i : socket.gethostbyname(i)}
    servers_result.update(servers_dict.items())
with open('test_json.json', 'w') as fp:
    json.dump(servers_result, fp)
with open('test_yaml.yaml', 'w') as file:
   file.write(yaml.dump(servers_result)) 