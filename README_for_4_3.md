# python_for_devops
# Домашнее задание к занятию "4.3. Языки разметки JSON и YAML"
### 1. Мы выгрузили JSON, который получили через API запрос к нашему сервису:
```json
{ "info" : "Sample JSON output from our service\t",
    "elements" :[
        { "name" : "first",
        "type" : "server",
        "ip" : 7175 
        },
        { "name" : "second",
        "type" : "proxy",
        "ip : 71.78.22.43
        }
    ]
}
```
### Нужно найти и исправить все ошибки, которые допускает наш сервис
### Ответ:
```json
{ "info" : "Sample JSON output from our service\t",
    "elements" :[
        { "name" : "first",
        "type" : "server",
        "ip" : 7175 
        },
        { "name" : "second",
        "type" : "proxy",
        "ip" : "71.78.22.43"
        }
    ]
}
```
### 2. В прошлый рабочий день мы создавали скрипт, позволяющий опрашивать веб-сервисы и получать их IP. К уже реализованному функционалу нам нужно добавить возможность записи JSON и YAML файлов, описывающих наши сервисы. Формат записи JSON по одному сервису: { "имя сервиса" : "его IP"}. Формат записи YAML по одному сервису: - имя сервиса: его IP. Если в момент исполнения скрипта меняется IP у сервиса - он должен так же поменяться в yml и json файле.
```python
#!/usr/bin/env python3
import socket
import json
import yaml

servers = {"drive.google.com" : "108.177.14.194", "mail.google.com" : "64.233.162.18", "google.com" : "173.194.220.113"}
servers_result = {}
for i, y in servers.items():
    servers_dict = {i : socket.gethostbyname(i)}
    servers_result.update(servers_dict.items())
with open('test_json.json', 'w') as fp:
    json.dump(servers_result, fp)
with open('test_yaml.yaml', 'w') as file:
   file.write(yaml.dump(servers_result)) 

```
### Полученные файлы в ветке мастер: test_json.json / test_yaml.yaml

