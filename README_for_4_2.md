# python_for_devops
# Домашнее задание к занятию "4.2. Использование Python для решения типовых DevOps задач"
### 1. Есть скрипт:
```python
#!/usr/bin/env python3
a = 1
b = '2'
c = a + b
```
### Какое значение будет присвоено переменной c? - Строка. Код не будет выполнен из-за разных типов значений.
### Как получить для переменной c значение 12? - добавить кавычки в переменную а
### Как получить для переменной c значение 3? - убрать кавычки из переменной b

### 2. Мы устроились на работу в компанию, где раньше уже был DevOps Engineer. Он написал скрипт, позволяющий узнать, какие файлы модифицированы в репозитории, относительно локальных изменений. Этим скриптом недовольно начальство, потому что в его выводе есть не все изменённые файлы, а также непонятен полный путь к директории, где они находятся. Как можно доработать скрипт ниже, чтобы он исполнял требования вашего руководителя?
```python
#!/usr/bin/env python3
import os

bash_command = ["cd /home/ubuntu/python_for_devops/python_for_devops", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
is_change = False
for result in result_os.split('\n'):
    if result.find('modified') != -1:
        prepare_result = result.replace('\tmodified:   ', '')
        print(prepare_result)
        #break
```
### 3.Доработать скрипт выше так, чтобы он мог проверять не только локальный репозиторий в текущей директории, а также умел воспринимать путь к репозиторию который мы передаём как входной параметр. Мы точно знаем, что начальство коварное и будет проверять работу этого скрипта в директориях, которые не являются локальными репозиториями.

```python
#!/usr/bin/env python3
import os
print(f'Введите путь до директории:')
path = input()
print(f'Поиск модифицированных файлов будет выполненен в директории: {path}')
cd_path = "cd " + path
bash_command = [cd_path, "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
is_change = False
for result in result_os.split('\n'):
    if result.find('modified') != -1:
        prepare_result = result.replace('\tmodified:   ', '')
        print(prepare_result)
```
### 4.Наша команда разрабатывает несколько веб-сервисов, доступных по http. Мы точно знаем, что на их стенде нет никакой балансировки, кластеризации, за DNS прячется конкретный IP сервера, где установлен сервис. Проблема в том, что отдел, занимающийся нашей инфраструктурой очень часто меняет нам сервера, поэтому IP меняются примерно раз в неделю, при этом сервисы сохраняют за собой DNS имена. Это бы совсем никого не беспокоило, если бы несколько раз сервера не уезжали в такой сегмент сети нашей компании, который недоступен для разработчиков. Мы хотим написать скрипт, который опрашивает веб-сервисы, получает их IP, выводит информацию в стандартный вывод в виде: <URL сервиса> - <его IP>. Также, должна быть реализована возможность проверки текущего IP сервиса c его IP из предыдущей проверки. Если проверка будет провалена - оповестить об этом в стандартный вывод сообщением: [ERROR] <URL сервиса> IP mismatch: <старый IP> <Новый IP>. Будем считать, что наша разработка реализовала сервисы: drive.google.com, mail.google.com, google.com.

```python
#!/usr/bin/env python3
import socket

servers = {"drive.google.com" : "108.177.14.194", "mail.google.com" : "64.233.162.18", "google.com" : "173.194.220.113"}
for i, y in servers.items():
    if y != socket.gethostbyname(i):
        print(f'[ERROR] {i} IP mismatch: {y} - {socket.gethostbyname(i)}')
    else:
        print(f'{i} {y} - {socket.gethostbyname(y)}')
```


